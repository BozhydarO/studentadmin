﻿using System.Windows;
using System.Windows.Input;
using University.Commands;
using University.Models;

namespace University.ViewModels
{
    /// <summary>
    /// Making all logic for InsertWindow
    /// </summary>
    public class InsertStudentViewModel : BaseViewModel
    {
        #region Private properties
        private readonly DbClass dbClass;
        private readonly Student studentClass;
        private string name = string.Empty;
        private string lastName = string.Empty;
        private string secondName = string.Empty;
        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        #endregion

        #region Constructor
        public InsertStudentViewModel()
        {
            this.dbClass = new DbClass();
            this.studentClass = new Student();
        }
        #endregion

        #region Public properties
        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                OnPropertyChanged(nameof(this.Name));
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                OnPropertyChanged(nameof(this.LastName));
            }
        }


        public string SecondName
        {
            get
            {
                return this.secondName;
            }

            set
            {
                this.secondName = value;
                OnPropertyChanged(nameof(this.SecondName));
            }
        }
        #endregion

        #region Public Comands
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }
        #endregion

        #region Private Methods
        private bool CanSave(object args)
        {
           return true;
        }

        private void Save(object args)
        {
            this.studentClass.Name = this.Name;
            this.studentClass.LastName = this.LastName;
            this.studentClass.SecondName = this.SecondName;

            bool isSuccessAddedContact = dbClass.InsertInoSrudentsTable(studentClass);
        
            if (isSuccessAddedContact)
            {
                MessageBox.Show("New student successful added!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed adding student!");
            }
        }
        private void Clear(object args)
        {
            this.Name = string.Empty;
            this.LastName = string.Empty;
            this.SecondName = string.Empty;
        }
        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        #endregion

    }
}