﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using University.Commands;
using University.Models;

namespace University.ViewModels
{
    /// <summary>
    /// Making all logic for MainWindow
    /// </summary>
    public class UpdateStudentViewModel : BaseViewModel
    {
        #region Private properties
        private DbClass dbClass;
        private Student studentClass;
        private string name = string.Empty;
        private string lastName = string.Empty;
        private string secondName = string.Empty;
        private List<Student> students;
        private int selectedStudentId;

        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        private ICommand selectedStudentChangedCommand;
        #endregion

        #region Constructor
        public UpdateStudentViewModel()
        {
            this.dbClass = new DbClass();
            this.Students = dbClass.GetStudents();
        }
        #endregion

        #region Public proterties

        public Student Student
        {
            get
            {
                return this.studentClass;
            }
            set
            {
                this.studentClass = value;
                this.OnPropertyChanged(nameof(this.Student));
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                this.name = value;
                OnPropertyChanged(nameof(this.Name));
            }
        }

        public string LastName
        {
            get
            {
                return this.lastName;
            }

            set
            {
                this.lastName = value;
                OnPropertyChanged(nameof(this.LastName));
            }
        }

        public string SecondName
        {
            get
            {
                return this.secondName;
            }

            set
            {
                this.secondName = value;
                OnPropertyChanged(nameof(this.SecondName));
            }
        }

        public List<Student> Students
        {
            get { return this.students; }

            set
            {
                this.students = value;
                this.OnPropertyChanged(nameof(this.Students));
            }
        }

        public int SelectedStudentId
        {
            get
            {
                return this.selectedStudentId;
            }
            set
            {
                this.selectedStudentId = value;
                OnPropertyChanged(nameof(this.SelectedStudentId));
            }
        }

        #endregion

        #region Public Commands

        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }

        public ICommand SelectedStudentChangedCommand
        {
            get
            {
                if (this.selectedStudentChangedCommand == null)
                {
                    this.selectedStudentChangedCommand = new RelayCommand<object>(this.SelectedStudentChanged);
                }

                return this.selectedStudentChangedCommand;
            }
        }

        #endregion

        #region Private Methods
        private bool CanSave(object args)
        {
            return true;
        }

        private void Save(object args)
        {
            Student.Name = this.name;
            Student.LastName = this.lastName;
            Student.SecondName = this.secondName;
            bool isSuccessUpdatedStudent = dbClass.UpdateStudent(Student);

            if (isSuccessUpdatedStudent)
            {
                MessageBox.Show("Student successful updated!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed updating student!");
            }
        }
        private void Clear(object args)
        {
            this.Name = string.Empty;
            this.LastName = string.Empty;
            this.SecondName = string.Empty;
        }

        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }

        private void SelectedStudentChanged(object args)
        {
            this.studentClass = dbClass.SearchStudentById(selectedStudentId);
            this.Name = studentClass.Name;
            this.LastName = studentClass.LastName;
            this.SecondName = studentClass.SecondName;

        }
        #endregion
    }
}
