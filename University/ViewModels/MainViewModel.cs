﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using University.Commands;
using University.Models;

namespace University.ViewModels
{
    /// <summary>
    /// Making all logic for MaintWindow
    /// </summary>
    public class MainViewModel : BaseViewModel
    {
        #region Private properties
        private readonly DbClass dbClass;
        private List<Subject> subjects;
        private List<Student> students;
        private int selectedStudentId;
        private int selectedSubjectId;
        #endregion

        #region Private commands
        private ICommand selectedStudentChangedCommand;
        private ICommand selectedSubjectChangedCommand;
        private ICommand detelteStudentCommand;
        private ICommand deleteSubjectCommand;
        private ICommand refreshCommand;
        private ICommand openInserStudenttWindowCommand;
        private ICommand openInserSubjecttWindowCommand;
        private ICommand openUpdateStudentWindowCommand;
        private ICommand openUpdateStubjectWindowCommand;
        #endregion

        #region Constructor
        public MainViewModel()
        {
            this.dbClass = new DbClass();
            Students = dbClass.GetStudents();
            Subjects = dbClass.GetSubjects();
        }
        #endregion

        #region Public proeprties
        public List<Student> Students
        {
            get { return this.students; }

            set
            {
                this.students = value;
                this.OnPropertyChanged(nameof(this.Students));
            }
        }

        public List<Subject> Subjects
        {
            get { return this.subjects; }
            set
            {
                this.subjects = value;
                this.OnPropertyChanged(nameof(this.Subjects));
            }
        }
        public int SelectedStudentId
        {
            get
            {
                return this.selectedStudentId;
            }
            set
            {
                this.selectedStudentId = value;
                OnPropertyChanged(nameof(this.SelectedStudentId));
            }
        }

        public int SelectedSubjectId
        {
            get
            {
                return this.selectedSubjectId;
            }
            set
            {
                this.selectedSubjectId = value;
                OnPropertyChanged(nameof(this.SelectedSubjectId));
            }
        }
        #endregion

        #region Public Commands

        public ICommand DetelteStudentCommand
        {
            get
            {
                if (this.detelteStudentCommand == null)
                {
                    this.detelteStudentCommand = new RelayCommand<object>(this.DeleteSutudent, this.CanDeleteStudent);
                }

                return this.detelteStudentCommand;
            }
        }

        public ICommand DetelteSubjectCommand
        {
            get
            {
                if (this.deleteSubjectCommand == null)
                {
                    this.deleteSubjectCommand = new RelayCommand<object>(this.DeleteSubject, this.CanDeleteSubject);
                }

                return this.deleteSubjectCommand;
            }
        }

        public ICommand DeleteDetailCommand
        {
            get
            {
                if (this.deleteSubjectCommand == null)
                {
                    this.deleteSubjectCommand = new RelayCommand<object>(this.DeleteSubject , this.CanDeleteSubject);
                }

                return this.deleteSubjectCommand;
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                if (this.refreshCommand == null)
                {
                    this.refreshCommand = new RelayCommand<object>(this.UpdateData);
                }
                return this.refreshCommand;
            }
        }

        public ICommand OpenInsertStudenttWindowCommand
        {
            get
            {
                if (this.openInserStudenttWindowCommand == null)
                {
                    this.openInserStudenttWindowCommand = new RelayCommand<object>(this.OpenInsertStudentsWindow);
                }

                return this.openInserStudenttWindowCommand;
            }
        }
        public ICommand OpenInsertStubjecttWindowCommand
        {
            get
            {
                if (this.openInserSubjecttWindowCommand == null)
                {
                    this.openInserSubjecttWindowCommand = new RelayCommand<object>(this.OpenInsertSubjectsWindow);
                }

                return this.openInserSubjecttWindowCommand;
            }
        }

        public ICommand OpenUpdateStudentWindowCommand
        { 
            get
            {
                if (this.openUpdateStudentWindowCommand == null)
                {
                    this.openUpdateStudentWindowCommand = new RelayCommand<object>(this.OpenUpdateStudentWindow);
                }

                return this.openUpdateStudentWindowCommand;
            }
        }

        public ICommand OpenUpdateSubjectWindowCommand
        {
            get
            {
                if (this.openUpdateStubjectWindowCommand == null)
                {
                    this.openUpdateStubjectWindowCommand = new RelayCommand<object>(this.OpenUpdateSubjectWindow);
                }

                return this.openUpdateStubjectWindowCommand;
            }
        }
        #endregion

        #region Private Methods

        private bool CanDeleteStudent(object args)
        {
            if(students == null)
            {
                return false;
            }

            return true;
        }

        private bool CanDeleteSubject(object args)
        {
            if (subjects == null)
            {
                return false;
            }

            return true;
        }

        private void DeleteSutudent(object args)
        {

         
            bool IsDeleteStudentSuccess = dbClass.DeleteStudent(selectedStudentId);

            if (IsDeleteStudentSuccess)
            {
                MessageBox.Show("Student is deleted successfull!");
                this.UpdateData(args);
            }
            else
            {
                MessageBox.Show("Cannot delete student!");
            }
        }
        private void DeleteSubject(object args)
        {
            bool IsDeleteSubjectSuccess = dbClass.DeleteSubject(selectedSubjectId);
            if (IsDeleteSubjectSuccess)
            {
                MessageBox.Show("Subject is deleted successfull!");
            }
            else
            {
                MessageBox.Show("Cannot delete subject!");
            }
        }

        private void UpdateData(object args)
        {
            this.Students = dbClass.GetStudents();
            this.Subjects = dbClass.GetSubjects();
        }

        private void OpenInsertStudentsWindow(object args)
        {
            InsertStudentWindow studentWindow = new InsertStudentWindow();
            studentWindow.Show();
        }
        private void OpenInsertSubjectsWindow(object args)
        {
            InsertSubjectWindow insertSubjectWindow = new InsertSubjectWindow();
            insertSubjectWindow.Show();
        }

        private void OpenUpdateStudentWindow(object args)
        {
            UpdateStudentWindow studentWindow = new UpdateStudentWindow();
            studentWindow.Show();
        }

        private void OpenUpdateSubjectWindow(object args)
        {
            UpdateSubjectWindow subjectWindow = new UpdateSubjectWindow();
            subjectWindow.Show();
        }
        #endregion
    }
}