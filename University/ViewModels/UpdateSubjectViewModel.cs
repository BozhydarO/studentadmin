﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using University.Commands;
using University.Models;

namespace University.ViewModels
{
    /// <summary>
    /// Making all logic for MainWindow
    /// </summary>
    public class UpdateSubjectViewModel : BaseViewModel
    {
        #region Private properties
        private DbClass dbClass;
        private Subject subjectClass;
        private string subjectName = string.Empty;
        private List<Subject> subjects;
        private int selectedSubjectId;

        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        private ICommand selectedSubjectChangedCommand;
        #endregion

        #region Constructor
        public UpdateSubjectViewModel()
        {
            this.dbClass = new DbClass();
            this.Subjects = dbClass.GetSubjects();
        }
        #endregion

        #region Public proterties

        public Subject Subject
        {
            get
            {
                return this.subjectClass;
            }
            set
            {
                this.subjectClass = value;
                this.OnPropertyChanged(nameof(this.Subject));
            }
        }

     

        public string SubjectName
        {
            get
            {
                return this.subjectName;
            }

            set
            {
                this.subjectName = value;
                OnPropertyChanged(nameof(this.SubjectName));
            }
        }

        public List<Subject> Subjects
        {
            get { return this.subjects; }

            set
            {
                this.subjects = value;
                this.OnPropertyChanged(nameof(this.Subjects));
            }
        }

        public int SelectedSubjectId
        {
            get
            {
                return this.selectedSubjectId;
            }
            set
            {
                this.selectedSubjectId = value;
                OnPropertyChanged(nameof(this.SelectedSubjectId));
            }
        }

        #endregion

        #region Public Commands

        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }

        public ICommand SelectedSubjectChangedCommand
        {
            get
            {
                if (this.selectedSubjectChangedCommand == null)
                {
                    this.selectedSubjectChangedCommand = new RelayCommand<object>(this.SelectedSubject);
                }

                return this.selectedSubjectChangedCommand;
            }
        }
        #endregion

        #region Private Methods
        private bool CanSave(object args)
        {
            return true;
        }

        private void Save(object args)
        {
            this.Subject.SebjectName = this.subjectName;
            bool isSuccessUpdatedStudent = dbClass.UpdateSubject(Subject);

            if (isSuccessUpdatedStudent)
            {
                MessageBox.Show("Student successful updated!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed updating student!");
            }
        }
        private void Clear(object args)
        {
            this.subjectName = string.Empty;
        }

        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }

        private void SelectedSubject(object args)
        {
            this.subjectClass = dbClass.SearchSubjectById(selectedSubjectId);
            this.subjectName = subjectClass.SebjectName;

        }
        #endregion
    }
}
