﻿using System.Windows;
using System.Windows.Input;
using University.Commands;
using University.Models;

namespace University.ViewModels
{
    /// <summary>
    /// Making all logic for InsertWindow
    /// </summary>
    public class InsertSubjectViewModel : BaseViewModel
    {
        #region Private properties
        //Instances of the classes
        private readonly DbClass dbClass;
        private readonly Subject subjectClass;

        //Needed properties from model
        private string subjectName = string.Empty;
        #endregion

        #region Private commands
        private ICommand saveCommand;
        private ICommand clearCommand;
        private ICommand closeCommand;
        #endregion

        #region Constructor
        public InsertSubjectViewModel()
        {
            //Initialization of the classes
            this.dbClass = new DbClass();
            this.subjectClass = new Subject();
        }
        #endregion

        #region Public properties

        //Initialization of all private properties
        public string SubjectName
        {
            get
            {
                return this.subjectName;
            }

            set
            {
                this.subjectName = value;
                OnPropertyChanged(nameof(this.SubjectName));
            }
        }
        #endregion

        #region Public Comands
        public ICommand SaveCommand
        {
            get
            {
                if (this.saveCommand == null)
                {
                    this.saveCommand = new RelayCommand<object>(this.Save, this.CanSave);
                }

                return this.saveCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (this.clearCommand == null)
                {
                    this.clearCommand = new RelayCommand<object>(this.Clear);
                }

                return this.clearCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new RelayCommand<Window>(this.Close);
                }

                return this.closeCommand;
            }
        }
        #endregion

        #region Private Methods

        //Can exdecute method for SaveCommand
        private bool CanSave(object args)
        {
           return true;
        }

        //Save method
        private void Save(object args)
        {
            this.subjectClass.SebjectName = this.SubjectName;

            bool isSuccessAddedContact = dbClass.InsertIntoSubjectsTable(subjectClass);
        
            if (isSuccessAddedContact)
            {
                MessageBox.Show("New subject successful added!");
                Clear(args);
            }
            else
            {
                MessageBox.Show("Failed adding subject!");
            }
        }

        //Clearing all TextBoxes when the button is clicked
        private void Clear(object args)
        {
            this.SubjectName = string.Empty;
        }

        //Closing InsertWindow when the button is clicked
        private void Close(Window window)
        {
            if (window != null)
            {
                window.Close();
            }
        }
        #endregion

    }
}