﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using University.ViewModels;

namespace University
{
    /// <summary>
    /// Interaction logic for InsertStudentWindow.xaml
    /// </summary>
    public partial class InsertStudentWindow : Window
    {
        public InsertStudentWindow()
        {
            InitializeComponent();
            this.DataContext = new InsertStudentViewModel();
        }
    }
}
