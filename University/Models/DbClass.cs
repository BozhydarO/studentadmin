﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace University.Models
{
    public class DbClass
    {
        //Connecting to DB
        static string connectonString = "Server=localhost; Port=3306; Database=university; Uid=root; Pwd=root";
        MySqlConnection connection = new MySqlConnection(connectonString);


        public List<Student> GetStudents()
        {
            List<Student> students = new List<Student>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM students ";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                Student student = null;

                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        student = new Student();
                        student.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        student.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                        student.Name = reader.GetString(reader.GetOrdinal("Name"));
                        student.SecondName = reader.GetString(reader.GetOrdinal("SecondName"));
                        students.Add(student);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return students;
        }


        public List<Subject> GetSubjects()
        {
            List<Subject> subjects = new List<Subject>();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM subjects";
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    Subject subject = null;
                    while (reader.Read())
                    {
                        subject = new Subject();
                        subject.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        subject.SebjectName = reader.GetString(reader.GetOrdinal("SubjectName"));
                        subjects.Add(subject);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return subjects;
        }

        public bool InsertInoSrudentsTable(Student student)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "INSERT INTO students(Name,LastName,SecondName)  VALUES (@Name , @LastName,@SecondName)";
                MySqlCommand command = new MySqlCommand(sql, connection);

                command.Parameters.AddWithValue("Id", student.Id);
                command.Parameters.AddWithValue("Name", student.Name);
                command.Parameters.AddWithValue("LastName", student.LastName);
                command.Parameters.AddWithValue("SecondName", student.SecondName);
                connection.Open();
                IsSuccess = command.ExecuteNonQuery() > 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }
            return IsSuccess;
        }
        public bool InsertIntoSubjectsTable(Subject subject)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "INSERT INTO subjects(SubjectName)  VALUES (@SubjectName)";
                MySqlCommand command = new MySqlCommand(sql, connection);
                command.Parameters.AddWithValue("Id", subject.Id);
                command.Parameters.AddWithValue("SubjectName", subject.SebjectName);
                connection.Open();
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                connection.Close();
            }
            return IsSuccess;
        }

        public bool UpdateStudent(Student student)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "UPDATE students SET Name = @Name, LastName = @LastName, SecondName = @SecondName WHERE Id = @Id";
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                command.Parameters.AddWithValue("@Id", student.Id);
                command.Parameters.AddWithValue("@Name", student.Name);
                command.Parameters.AddWithValue("@LastName", student.LastName);
                command.Parameters.AddWithValue("@SecondName", student.SecondName);
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;
        }


        public bool UpdateSubject(Subject Subject)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);

            try
            {
                string sql = "UPDATE subjects SET SubjectName = @SubjectName WHERE Id =@Id";
                MySqlCommand command = new MySqlCommand(sql, connection);
                connection.Open();
                command.Parameters.AddWithValue("@Id", Subject.Id);
                command.Parameters.AddWithValue("@SubjectName", Subject.SebjectName);
                IsSuccess = command.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;
        }


        public bool DeleteStudent(int Id)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingContact = new MySqlCommand("delete from students where students.Id =" + Id, connection);
                connection.Open();
                IsSuccess = commandForDeletingContact.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }


        public bool DeleteSubject(int Id)
        {
            bool IsSuccess = false;
            connection = new MySqlConnection(connectonString);
            try
            {
                MySqlCommand commandForDeletingDetail = new MySqlCommand("delete from subjects where Id = " + Id + " ", connection);
                connection.Open();
                IsSuccess = commandForDeletingDetail.ExecuteNonQuery() > 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return IsSuccess;

        }

        public Student SearchStudentById(int id)
        {
            Student student = new Student();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM students " +
                         "where students.Id = " + id;
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        student = new Student();
                        student.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        student.SecondName = reader.GetString(reader.GetOrdinal("SecondName"));
                        student.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                        student.Name = reader.GetString(reader.GetOrdinal("Name"));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return student;
        }

        public Subject SearchSubjectById(int id)
        {
            Subject subject = new Subject();
            connection = new MySqlConnection(connectonString);
            try
            {
                string sql = "SELECT * FROM subjects " +
                         "where subjects.Id = " + id;
                MySqlCommand sqlCommand = new MySqlCommand(sql, connection);
                connection.Open();
                using (var reader = sqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        subject = new Subject();
                        subject.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        subject.SebjectName = reader.GetString(reader.GetOrdinal("SubjectName"));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exeption", ex);
            }
            finally
            {
                connection.Close();
            }

            return subject;
        }
    }
}
